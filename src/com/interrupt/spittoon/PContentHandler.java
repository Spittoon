package com.interrupt.spittoon;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

public class PContentHandler implements ContentHandler, LexicalHandler, DeclHandler { 
	
	
	Logger logger = Logger.getLogger(PContentHandler.class); 
    public void startDocument() throws SAXException{ 
    	logger.debug("startDocument CALLED"); 
    }

    public void endDocument() throws SAXException { 
    	logger.debug("endDocument CALLED"); 
    }
    
    public void ignorableWhitespace(char[] ch, int start, int length) {
    	logger.debug("ignorableWhitespace CALLED"); 
    }

    public void characters(char[] ch, int start, int length) {
    	logger.debug("characters CALLED"); 
    }
    
    public void startCDATA() {
    	logger.debug("startCDATA CALLED"); 
    }

    public void endCDATA() {
    	logger.debug("endCDATA CALLED"); 
    }
    
    
    // called immediately before the 'startElement' event 
    public void startPrefixMapping (String prefix, String uri) throws SAXException{
    	logger.debug("startPrefixMapping CALLED"); 
    } 
    
    // called immediately after the 'endElement' event 
    public void endPrefixMapping (String prefix) throws SAXException { 
    	logger.debug("endPrefixMapping CALLED"); 
    }
    
    public void startElement (String namespaceURI, String localName,
        String qName, Attributes atts) throws SAXException { 
    	
    	logger.debug("startElement CALLED / namespace["+namespaceURI+"] localName["+localName+"] qname["+qName+"] attributes["+atts+"]"); 
    }
    public void endElement(String namespaceURI, String localName, String qName) { 
    	logger.debug("endElement CALLED / namespace["+namespaceURI+"] localName["+localName+"] qname["+qName+"]"); 
    }
    
    public void setDocumentLocator(org.xml.sax.Locator locator) {
    } 
    public void processingInstruction (String target, String data) throws SAXException{
    }
    public void skippedEntity (String name) throws SAXException{
    }
    
    
    /*  Lexical handler methods
     */
    public void startDTD (String name, String publicId, String systemId) throws SAXException{
    }
    public void endDTD () throws SAXException{
    }
    public void startEntity (String name) throws SAXException{
    }
    public void endEntity (String name) throws SAXException{
    }
	public void comment (char ch[], int start, int length) throws SAXException{
	}
     
	/* DeclHandler methods
	 */
	public void elementDecl(String name, String model) throws SAXException {
	}
	public void attributeDecl(String eName, String aName, String type, String valueDefault, String value) throws SAXException {
	}
	public void internalEntityDecl (String name, String value) throws SAXException {
	}
	public void externalEntityDecl (String name, String publicId, String systemId) throws SAXException {
	}
    
}

