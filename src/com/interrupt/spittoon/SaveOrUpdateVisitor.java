package com.interrupt.spittoon;

import org.apache.log4j.Logger;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.interrupt.bob.base.IBob;
import com.interrupt.bob.base.IVisitor;

public class SaveOrUpdateVisitor implements IVisitor {
	
	
	private Logger logger = Logger.getLogger(SaveOrUpdateVisitor.class); 
	private Spittoon spittoon = null; 
	private String dbUrl = null; 
	private String parentXPath = null; 
	
	public SaveOrUpdateVisitor(Spittoon spittoon) { 
		this.spittoon = spittoon; 
	}
	
	public Spittoon getSpittoon() { return spittoon; }
	public void setSpittoon(Spittoon spittoon) { this.spittoon = spittoon; }
	public String getDbUrl() { return dbUrl; }
	public void setDbUrl(String dbUrl) { this.dbUrl = dbUrl; }
	public String getParentXPath() { return parentXPath; }
	public void setParentXPath(String parentXPath) { this.parentXPath = parentXPath; } 
	
	
	public void visit(IBob bob) { 
		
		bob.setProperty(IBob.XML_DEPTH, "1"); //** just print out the parent document 
	    String xml = bob.toXML(false); 
		
		String xpath = bob.xpath(false); 
		String tagName = bob.getTagName(); 
		tagName = tagName.trim(); 
		logger.debug("Visiting["+ xml +"] > parentXPath["+ parentXPath +"] > xpath["+ xpath +"]"); 
		
		if( parentXPath != null ) 
			xpath = parentXPath + xpath; 
		
		//** check if xpath is mapped 
		String bareXPath = spittoon.stripXPath(xpath); 
		//if(!spittoon.verifyXPathMapping(bareXPath)) { 
		//	logger.debug("XPath mapping ["+ bareXPath +"] not registered. Skipping ["+ bob.getTagName() +"]"); 
		//	return; 
		//}
		
		//** find collection from xpath 
		Collection collection = spittoon.getCollectionForXPath(dbUrl, bareXPath); 
		logger.debug("Retrieved collection["+ collection +"]"); 
		if(collection == null) { 
			
			//** generate collection 
			logger.debug("Collection is NULL > generating collection for xpath["+xpath+"]..."); 
			collection = spittoon.generateCollectionFromXPath(dbUrl, xpath); 
			
			try { 
				
				logger.debug("Generated collection ["+ collection.getName() +"]"); 
			}
			catch(XMLDBException e) { 
				throw new SpittoonException(e); 
			}
		}
		
		String resourceName = spittoon.generateResourceName(bob); 
		
		try { 
			
			//** check if document exists 
			logger.debug("Checking if resource["+resourceName+"] exists in collection["+collection.getName()+"]"); 
			XMLResource document = (XMLResource)collection.getResource(resourceName); 
			if(document == null) { 
				
				document = (XMLResource)collection.createResource(resourceName, "XMLResource"); 
				logger.debug("Resource does NOT exist > created resource["+document.getId()+"] in collection["+collection.getName()+"]"); 
			}
			document.setContent(xml); 
	        
			//** save document 
			logger.debug("SaveOrUpdateVisitor.visit:: Storing document resource > " + document.getId() + " > content["+document.getContent()+"]");  
	        
	        collection.storeResource(document); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		catch(ClassCastException e) { 
			throw new SpittoonException(e); 
		}
		
	}
	
}
