package com.interrupt.spittoon;

public class SpittoonException extends RuntimeException {
	
	SpittoonException() { 
		super(); 
	}
	SpittoonException(String message) { 
		super(message); 
	}
	SpittoonException(String message, Throwable cause) { 
		super(message, cause); 
	}
	SpittoonException(Throwable cause) { 
		super(cause); 
	}
	
}
