package com.interrupt.spittoon;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;
import org.apache.log4j.Logger;
import org.exist.EXistException;
import org.exist.storage.BrokerPool;
import org.exist.util.Configuration;
import org.exist.util.DatabaseConfigurationException;

import com.interrupt.bob.base.Bob;
import com.interrupt.bob.base.IBob;
import com.interrupt.bob.util.Util;


public class Spittoon {
	
	
	private String DRIVER = "org.exist.xmldb.DatabaseImpl"; 
	private Properties props = new Properties(); 
	private Properties mappings = new Properties(); 
	
	private String rawDbUrl = null; 
	private String aauthDbUrl = null; 
	private String groupsDbUrl = null; 
	
	private String rootDir = null; 
	private String uname = null; 
	private String passwd = null; 
	
	private Logger logger = Logger.getLogger(Spittoon.class); 
	private Logger algorithm = Logger.getLogger("algorithm"); 
	
	private Database databaseAauth = null; 
	private Database databaseGroups = null; 
	private Database database = null; 
	
	private String einitDb = null; 
	
	
	public String getAauthDbUrl() { return aauthDbUrl; }
	public void setAauthDbUrl(String aauthDbUrl) { this.aauthDbUrl = aauthDbUrl; }
	public String getGroupsDbUrl() { return groupsDbUrl; }
	
	
	public void setGroupsDbUrl(String groupsDbUrl) {
		this.groupsDbUrl = groupsDbUrl;
	}

	public void initialise() { 
		
		try {
			
		    //** load spittoon.properties 
		    InputStream inStream = Spittoon.class.getResourceAsStream("/spittoon.properties"); 
		    props.load(inStream); 
		    
		    einitDb = System.getProperty("exist.initdb"); 
		    if(einitDb == null || einitDb.trim().length() < 1) 
		    	einitDb = props.getProperty("exist.initdb"); 
		    
		    String ehome = System.getProperty("exist.home"); 
		    if(ehome == null || ehome.trim().length() < 1) 
		    	ehome = props.getProperty("exist.home"); 
		    
		    String eSaxValidation = System.getProperty("org.xml.sax.features.validation");
		    if(eSaxValidation == null || eSaxValidation.trim().length() < 1) 
		    	eSaxValidation = props.getProperty("org.xml.sax.features.validation"); 
		    
		    String eEndorsedDirs = System.getProperty("java.endorsed.dirs"); 
		    if(eEndorsedDirs == null || eEndorsedDirs.trim().length() < 1) 
		    	eEndorsedDirs = props.getProperty("java.endorsed.dirs"); 
		    
		    
		    logger.debug("exist.initdb["+einitDb+"] / exist.home["+ehome+
		    		"] / org.xml.sax.features.validation["+eSaxValidation+"] / java.endorsed.dirs["+eEndorsedDirs+"]"); 
		    
		    String db1Config = props.getProperty("db-1-config");
		    String db1Id = props.getProperty("db-1-id").trim();
		    String db1Port = props.getProperty("db-1-port"); 
		    
		    String db2Config = props.getProperty("db-2-config").trim();
		    String db2Id = props.getProperty("db-2-id");
		    String db2Port = props.getProperty("db-2-port"); 
		    
		    String dbSplit = props.getProperty("db-split");
		    
		    logger.debug("Database / split["+ dbSplit +"]"); 
		    logger.debug("Database / config-1["+ db1Config +"] / id-1["+ db1Id +"] / port-1["+db1Port+"]"); 
		    logger.debug("Database / config-2["+ db2Config +"] / id-2["+ db2Id +"] / port-2["+db2Port+"]"); 
		    
		    
		    
		    //** initialize driver 
		    Class cl = Class.forName(DRIVER); 
		    
		    System.setProperty("exist.initdb", einitDb); 
		    System.setProperty("exist.home", ehome); 
		    System.setProperty("org.xml.sax.features.validation", eSaxValidation); 
		    System.setProperty("java.endorsed.dirs", eEndorsedDirs); 
		    
		    //** extract the mappings into a separate properties object 
		    extractMappings(); 
		    
		    
			//** eXist Broker Pool properties; initialize from properties 
		    rawDbUrl = props.getProperty("db.url"); 
			rootDir = props.getProperty("root.dir", "root.dir"); 
			uname = props.getProperty("db.uname"); 
			passwd = props.getProperty("db.passwd"); 
			
			rawDbUrl = rawDbUrl.trim(); 
			rootDir = rootDir.trim(); 
			uname = uname.trim(); 
			passwd = passwd.trim(); 
			
			if(einitDb.equals("true")) { 
				
				databaseAauth = (Database)cl.newInstance(); 
			    databaseAauth.setProperty("create-database", einitDb);  
			    databaseAauth.setProperty("configuration", db1Config);  
			    databaseAauth.setProperty("database-id", db1Id); 
			    DatabaseManager.registerDatabase(databaseAauth); 
			    logger.debug("EMBEDDED Database registered["+ databaseAauth.getName() +"]"); 
			    
			    databaseGroups = (Database)cl.newInstance(); 
			    databaseGroups.setProperty("create-database", einitDb);  
			    databaseGroups.setProperty("configuration", db2Config);  
			    databaseGroups.setProperty("database-id", db2Id);  
			    DatabaseManager.registerDatabase(databaseGroups); 
			    logger.debug("EMBEDDED Database registered["+ databaseGroups.getName() +"]"); 
			    
				aauthDbUrl = rawDbUrl.replaceFirst("exist", db1Id); 
				groupsDbUrl = rawDbUrl.replaceFirst("exist", db2Id); 
			}
			else { 
				
				//aauthDbUrl = rawDbUrl.replaceFirst("exist", db1Id); 
				//groupsDbUrl = rawDbUrl.replaceFirst("exist", db2Id); 
				//aauthDbUrl = aauthDbUrl.replaceFirst("8080", db1Port); 
				//groupsDbUrl = groupsDbUrl.replaceFirst("8080", db2Port);
				
				database = (Database)cl.newInstance(); 
			    DatabaseManager.registerDatabase(database); 
			    logger.debug("REMOTE Database registered["+ database.getName() +"]"); 
			    
			    aauthDbUrl = rawDbUrl.replaceFirst("8080", db1Port); 
				groupsDbUrl = rawDbUrl.replaceFirst("8080", db2Port); 
			}
			
			logger.debug("RawDBURL["+rawDbUrl+"] / ROOT["+rootDir+"] / UNAME["+uname+"] / PASSWD["+passwd+"]"); 
			logger.debug("Aauth DBURL["+aauthDbUrl+"] / Groups DBURL["+groupsDbUrl+"]");  
			logger.debug("exist.initdb > " + System.getProperty("exist.initdb")); 
			 
	    	Configuration configuration1 = null; 
	    	try { 
	    		
	    		configuration1 = new Configuration(db1Config); 
	    	}
	    	catch(org.exist.util.DatabaseConfigurationException e) { 
	    		
	    		logger.error("ERROR in configuration ["+e.getMessage()+"] with file["+db1Config+"]"); 
	    		
	    		// On error, try Tomcat specific context 
	    		String catalinaBase = System.getProperty("catalina.base"); 
	    		configuration1 = new Configuration( catalinaBase + "/webapps/webkell/" + db1Config ); 
	    	}
	    	BrokerPool.configure( 1, 5, configuration1 ); 
			
	    	if(dbSplit.equals("true")) { 
				Configuration configuration2 = new Configuration(db2Config); 
				BrokerPool.configure( 1, 5, configuration2 ); 
			}
			 
	    	
			//** set Bob properties 
			System.setProperty(Util.HOME, props.getProperty("bob.home"));
		    System.setProperty(Util.BASE, props.getProperty("bob.base"));
		    System.setProperty(Util.GEN, props.getProperty("bob.gen"));
		    System.setProperty(Util.END, props.getProperty("bob.end"));
		    System.setProperty(Util.DEF, props.getProperty("bob.def"));
			
		}
		catch(DatabaseConfigurationException e) { 
			e.printStackTrace(); 
    	}
    	catch(EXistException e) { 
    		e.printStackTrace(); 
    	}
    	catch(Exception e) { 
		    e.printStackTrace(); 
		}
		
	}
	
	public void setupRoot() throws SpittoonException { 
		
	    String db1Id = props.getProperty("db-1-id");
	    String db2Id = props.getProperty("db-2-id");
	    
		String aauthDbUrl = rawDbUrl.replaceAll("exist", db1Id); 
		String groupsDbUrl = rawDbUrl.replaceAll("exist", db2Id); 
		
		createCollection(aauthDbUrl, rootDir); 
		createCollection(groupsDbUrl, rootDir); 
		
	}
	public void tearDownRoot() throws SpittoonException { 
		
		removeCollection(aauthDbUrl, rootDir); 
		removeCollection(groupsDbUrl, rootDir); 
		
		try { 
			
			if(einitDb.equals("true")) { 
				
				DatabaseManager.deregisterDatabase(databaseAauth); 
			    logger.debug("De-registered EMBEDED Database ["+ databaseAauth.getName() +"]"); 
			    
			    DatabaseManager.deregisterDatabase(databaseGroups); 
			    logger.debug("De-registered EMBEDED atabase ["+ databaseGroups.getName() +"]"); 
			}
			else { 
				
				DatabaseManager.deregisterDatabase(database); 
			    logger.debug("De-registered REMOTE Database ["+ database.getName() +"]"); 
			}
		}
		catch(Exception e) { 
			e.printStackTrace(); 
		}
	}
	private Collection getCollection(String dbdirectory) throws SpittoonException { 
		
		logger.debug("Retrieving collection for URL["+ dbdirectory +"]");
		Collection dbCollection = null; 
		try { 
			
			dbCollection = DatabaseManager.getCollection(dbdirectory, uname, passwd); 
            logger.debug("Retrieved collection["+ dbCollection.getName() +"]"); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		
		return dbCollection; 
	}
	private Collection createCollection(String dburl, String dirname) throws SpittoonException { 
		
		Collection dbCollection = null; 
		Collection col = null; 
		try { 
			
			logger.debug("createCollection for dburl["+ dburl +"]"); 
			
			dbCollection = DatabaseManager.getCollection(dburl, uname, passwd); 
			CollectionManagementService mgtService = (CollectionManagementService)
                dbCollection.getService("CollectionManagementService", "1.0");  
            col = mgtService.createCollection(dirname); 
            logger.debug("Created directory["+ col.getName() +"]"); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		
		return col; 
	}
	public void removeCollection(String dburl, String dirname) throws SpittoonException { 
		
		Collection dbCollection = null; 
		try { 
			
			dbCollection = DatabaseManager.getCollection(dburl, uname, passwd); 
			CollectionManagementService mgtService = (CollectionManagementService)
                dbCollection.getService("CollectionManagementService", "1.0");  
            mgtService.removeCollection(dirname); 
            logger.debug("Removed directory["+ dburl + dirname +"]"); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		
	}
	
	
	private void extractMappings() { 
		
		/**
		 * deal with a path that is not mapped 
		 */
		Set keys = props.keySet();  
		Iterator kiter = keys.iterator(); 
		String nextKey = null;
		while(kiter.hasNext()) { 
			
			nextKey = (String)kiter.next(); 
			if(nextKey.startsWith("mapping")) { 
				mappings.setProperty(nextKey, props.getProperty(nextKey)); 
			}
		}
	}
	
	
	public boolean alive() throws SpittoonException { 
		
		
		users: { 
			
			/** 
			 * generate root collection URL string 
			 */
			StringBuilder sbuffer = new StringBuilder(); 
			sbuffer.append(aauthDbUrl);
			if(!aauthDbUrl.endsWith("/")) { 
				sbuffer.append("/"); 
			}
			sbuffer.append(rootDir);
			String rootCollection = sbuffer.toString(); 
			
			/** 
			 * see if we have the exist 'db' collection 
			 */
			Collection dbCollection = null; 
			try { 
				dbCollection = DatabaseManager.getCollection(aauthDbUrl, uname, passwd); 
				if(dbCollection == null) { 
					throw new SpittoonException("Cannot find groups 'db' directory"); 
				}
			}
			catch(XMLDBException xe) { 
				throw new SpittoonException(xe.getMessage(), xe.getCause()); 
			}
			
			/** 
			 * test to see if the root collection exists
			 */
			Collection currentCollection = null; 
			try { 
				currentCollection = DatabaseManager.getCollection(rootCollection, uname, passwd);
				if(currentCollection == null) 
					throw new SpittoonException("root collection for aauth DB is NULL"); 
			}
			catch(XMLDBException xe) { 
				throw new SpittoonException(xe.getMessage(), xe.getCause()); 
			}
		}
		groups: { 
			
			
			/** 
			 * generate root collection URL string 
			 */
			StringBuilder sbuffer = new StringBuilder(); 
			sbuffer.append(groupsDbUrl);
			if(!groupsDbUrl.endsWith("/")) { 
				sbuffer.append("/"); 
			}
			sbuffer.append(rootDir);
			String rootCollection = sbuffer.toString(); 
			
			
			/** 
			 * see if we have the exist 'db' collection 
			 */
			Collection dbCollection = null; 
			try { 
				dbCollection = DatabaseManager.getCollection(groupsDbUrl, uname, passwd); 
				if(dbCollection == null) { 
					throw new SpittoonException("Cannot find groups 'db' directory"); 
				}
			}
			catch(XMLDBException xe) { 
				throw new SpittoonException(xe.getMessage(), xe.getCause()); 
			}
			
			/** 
			 * test to see if the root collection exists
			 */
			Collection currentCollection = null; 
			try { 
				currentCollection = DatabaseManager.getCollection(rootCollection, uname, passwd);
				if(currentCollection != null) 
					return true; 
			}
			catch(XMLDBException xe) { 
				throw new SpittoonException(xe.getMessage(), xe.getCause()); 
			}
		}
		return false; 
		
	}
	
	
	public void create(String dbUrl, String xpath, String xml) throws SpittoonException {
		
		
		logger.debug("Spittoon.create CALLED / dbUrl["+dbUrl+"] / xpath["+xpath+"] / xml["+xml+"]"); 
		
		//** check if xpath is mapped 
		String bareXPath = stripXPath(xpath); 
		//if(!verifyXPathMapping(bareXPath)) { 
		//	throw new SpittoonException("XPath mapping ["+ bareXPath +"] not registered"); 
		//}
		
		//** load IBob from xml 
		IBob loadedBob = loadBobFromXML(xml); 
		String tagName = loadedBob.getTagName(); 
		tagName = tagName.trim(); 
		
		
		//** check if we're saving the right tag
		String targetTag = bareXPath.substring( (bareXPath.lastIndexOf("/") + 1) ); 
		targetTag = targetTag.trim(); 
		logger.debug("Comparing tagName["+tagName+"] to targetTag["+targetTag+"]"); 
		if(!tagName.equals(targetTag)) 
			throw new SpittoonException("XML supplied does not match the target xpath"); 
		
		
		//** find collection from xpath 
		Collection collection = getCollectionForXPath(dbUrl, bareXPath); 
		logger.debug("Retrieved collection["+ collection +"]"); 
		if(collection == null) { 
			
			//** generate collection 
			logger.debug("Collection is NULL / generating collection..."); 
			collection = generateCollectionFromXPath(dbUrl, xpath); 
			logger.debug("Generated collection ["+ collection +"]"); 
		}
		
		String resourceName = generateResourceName(loadedBob); 
		try { 
			
			//** check if document exists 
			logger.debug("Checking if resource["+resourceName+"] exists in collection["+collection.getName()+"]"); 
			XMLResource document = (XMLResource)collection.getResource(resourceName); 
			if(document == null) { 
				
				document = (XMLResource)collection.createResource(resourceName, "XMLResource"); 
				logger.debug("Resource does NOT exist / created resource["+document.getId()+"] in collection["+collection.getName()+"]"); 
			}
			document.setContent(loadedBob.toXML(false)); 
	        
			//** save document 
			logger.debug("testing 123 - loadedBob.toXML["+loadedBob.toXML(false)+"]"); 
	        logger.debug("Spittoon.create:: Storing document resource > " + document.getId() + " > content["+document.getContent()+"]");  
	        collection.storeResource(document); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		catch(ClassCastException e) { 
			throw new SpittoonException(e); 
		}
		
	}
	
	
	public void createR(String dbUrl, String xpath, String xml) throws SpittoonException {
		
		
		logger.debug("Spittoon.createR CALLED / dbUrl["+ dbUrl +"] / xpath["+ xpath +"] / xml["+ xml +"]"); 
		
		//** check if xpath is mapped 
		String bareXPath = stripXPath(xpath); 
		//if(!verifyXPathMapping(bareXPath)) { 
		//	throw new SpittoonException("XPath mapping ["+ bareXPath +"] not registered"); 
		//}
		
		
		//** load IBob from xml 
		IBob loadedBob = loadBobFromXML(xml); 
		String tagName = loadedBob.getTagName(); 
		tagName = tagName.trim(); 
		
		
		//** check if we're saving the right tag
		String targetTag = bareXPath.substring( (bareXPath.lastIndexOf("/") + 1) ); 
		targetTag = targetTag.trim(); 
		logger.debug("Comparing tagName["+tagName+"] to targetTag["+targetTag+"]"); 
		if(!tagName.equals(targetTag)) 
			throw new SpittoonException("XML supplied does not match the target xpath"); 
		
		//**
		this.generateCollectionFromXPath(dbUrl, xpath); 
		
		//** recurse through tree and save 
		List results = loadedBob.find(xpath); 
		IBob targetBob = null; 
		if(!results.isEmpty()) {	// is the XML the entire block from 'system' 
			targetBob = (IBob)results.get(0); 
		}
		else { 
			
			targetBob = loadedBob; 
			//String xtraPath = generateCollectionNameR(xpath, bareXPath); 
			//dbUrl += xtraPath; 
			//logger.debug("targetBob = loadedBob / xtraPath["+xtraPath+"] / dbUrl["+dbUrl+"]"); 
		}
		
		logger.debug("Running SaveOrUpdateVisitor on target["+ targetBob.toXML(false) +"]"); 
		
		SaveOrUpdateVisitor suVisitor = new SaveOrUpdateVisitor(this); 
		//if(!results.isEmpty()) {
			suVisitor.setParentXPath(
				xpath.substring( 0, 
					(xpath.lastIndexOf("/") + 1) )); 
		//}
		suVisitor.setDbUrl(dbUrl); 
		
		targetBob.acceptFirst(suVisitor);  
		
	}
	
	
	private Collection getCollectionForFullXPath(String dbUrl, String xpath) { 
		
		String bareXPath = stripXPath(xpath); 
		logger.debug("getCollectionForFullXPath:: xpath["+xpath+"] > bareXPath["+bareXPath+"]"); 
		
		//** generate collection name 
		String colName = generateCollectionNameR(xpath, bareXPath); 
		logger.debug("getCollectionForFullXPath:: generated collection name["+ colName +"]"); 
		
		//** get collection
		String dbdirectory = dbUrl + rootDir +"/"+ colName; 
		Collection collection = this.getCollection(dbdirectory); // use full URL 
		logger.debug("getCollectionForFullXPath:: retrieved collection["+collection+"]"); 
		
		return collection; 
	}
	public IBob retrieve(String dbUrl, String xpath, boolean recurse) { 
		
		algorithm.debug(""); algorithm.debug(""); 
		algorithm.debug("retrieve CALLED > params("+dbUrl+","+xpath+","+recurse+")"); 
		
		/****** 
		 * check if there's a child document 
		 *
		String bareXPath = stripXPath(xpath); 
		logger.debug("retrieve:: xpath["+xpath+"] > bareXPath["+bareXPath+"]"); 
		
		//** generate collection name 
		String colName = generateCollectionNameR(xpath, bareXPath); 
		logger.debug("retrieve:: generated collection name["+ colName +"]"); 
		
		//** get collection
		String dbdirectory = dbUrl + rootDir +"/"+ colName; 
		Collection collection = this.getCollection(dbdirectory); // use full URL 
		*/
		
		/****** 
		 * check if there's a child document 
		 */
		logger.debug("Retrieving for > dbUrl["+ dbUrl +"] / xpath["+ xpath +"]"); 
		Collection collection = getCollectionForFullXPath(dbUrl, xpath); 
		
		//** child resource will be named the same as the directory 
		IBob resultBob = null; 
		try { 
			
			String rname = collection.getName(); 
			rname = rname.substring(rname.lastIndexOf("/") + 1); 
			XMLResource eachResource = (XMLResource)collection.getResource(rname); 
			algorithm.debug("retrieve > Child resource ["+ eachResource.getContent() +"] > forName["+rname+"] > inCollection["+collection.getName()+"]"); 
			
			if(eachResource == null) { 
				throw new SpittoonException("NULL child resource in directory ["+ collection.getName()+"]"); 
			}
			
			/****** 
			 * if yes, then i) add to parent 'bob' ii) set as parent 'bob' 
			 */
			resultBob = this.loadBobFromXML((String)eachResource.getContent()); 
			algorithm.debug("retrieve > LOADED Child resource ["+ resultBob +"] > xpath["+ resultBob.xpath(true) +"]"); 
			
			/*if(parent != null) { 
				
				FindParentBob fpvisitor = new FindParentBob(); 
				fpvisitor.setTagName(resultBob.getTagName()); 
				fpvisitor.setAttributeValue(resultBob.getAttributeValue("id")); 
				fpvisitor.setReplacement(resultBob); 
				parent.acceptFirst(fpvisitor); //** i) 
				
				logger.debug("OK > new parent after replace... " + parent.toXML(false)); 
			}
			*/
			
			
			//** should we recurse 
			if(recurse) { 
				
				algorithm.debug("retrieve > IN recurse block"); 
				
				//** get child collections 
				String[] childCollections = collection.listChildCollections(); 
				IBob eachBob = null; 
				for (String string : childCollections) {
					
					algorithm.debug("retrieve > EACH "); 
					
					//** for each child collection, recurse to it's children 
					String tname = string.substring(0, string.indexOf(".")); 
					String idvalue = string.substring(string.indexOf(".") + 1); 
					
					StringBuilder sbuf = new StringBuilder(xpath); 
					sbuf.append("/"); 
					sbuf.append(tname); 
					sbuf.append("[ "); 
					sbuf.append("@id='"); 
					sbuf.append(idvalue); 
					sbuf.append("' ]"); 
					
					//** next XML retrieved 
					String eachXPath = sbuf.toString(); 
					algorithm.debug("retrieve > EACH childCollection string["+string+"] > created XPath["+eachXPath+"]"); 
					
					algorithm.debug("IN recurse");
					eachBob = retrieve(dbUrl, eachXPath, recurse); //** ii) 
					algorithm.debug("OUT recurse");
					
					boolean removed = resultBob.remove(eachBob.getTagName(), "id", eachBob.getAttributeValue("id")); 
					algorithm.debug("["+removed+"] removed '"+eachBob.getTagName()+"[@id='"+eachBob.getAttributeValue("id")+"']");
					
					resultBob.addChild(eachBob); 
					
				}
			}
			
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e); 
		}
		
		return resultBob; 
	}
	
	public Collection getCollectionForXPath(String dbUrl, String bareXPath) throws SpittoonException { 
		
		
		//if(!verifyXPathMapping(bareXPath)) { 
		//	throw new SpittoonException("XPath ["+ bareXPath +"] is not mapped"); 
		//}
		
		String eurl = generateDbUrl((dbUrl + rootDir), bareXPath); 
		logger.debug("getCollectionForXPath:: Generated eurl["+ eurl +"]"); 
		
		Collection collection = null; 
		try { 
			collection = DatabaseManager.getCollection(eurl, uname, passwd); 
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e.getMessage(), e); 
		}
		
		return collection; 
	}
	
	public Collection generateCollectionFromXPath(String dbUrl, String xpath) throws SpittoonException { 
		
		logger.debug("Spittoon.generateCollectionFromXPath:: dbUrl["+ dbUrl +"] / xpath["+ xpath +"]"); 
		String bareXPath = stripXPath(xpath); 
		
		//if(!verifyXPathMapping(bareXPath)) { 
		//	throw new SpittoonException("XPath ["+ bareXPath +"] is not mapped"); 
		//}
		
		String baseName = dbUrl + rootDir; 
		String eurl = dbUrl + rootDir; 
		logger.debug("generateCollectionFromXPath:: Generated rootUrl["+ eurl +"]"); 
		
		
		StringTokenizer stokenizer = new StringTokenizer( bareXPath, "/" ); 
		StringTokenizer xpathTokenizer = new StringTokenizer( xpath, "/" ); //** mirroring stokenizer in loop ** It's bad, I know, I knooowwww **
		Collection nextCollection = null; 
		try { 
			
			//** this should be the root collection
			nextCollection = DatabaseManager.getCollection(eurl, uname, passwd); 
			//logger.debug("1. Next collection["+ nextCollection +"]"); 
			if(nextCollection == null)
				throw new SpittoonException("There is no root collection["+ eurl +"]"); 
			
			String nextToken = stokenizer.nextToken(); 
			String nextXPath = xpathTokenizer.nextToken(); 
			String previousUrl = null; 
			
			//logger.debug("1. nextToken ["+ nextToken +"] / nextXPath["+ nextXPath +"]"); 
			
			boolean moreTokens = false; 
			if(nextToken != null)
				moreTokens = true; 
			
			
			//String checkMappingS = ""; 
			
			while((nextCollection != null) && moreTokens) { 
				
				previousUrl = eurl; 
				
				logger.debug("nextToken["+nextToken +"] / nextXPath["+ nextXPath +"]"); 
				String colName = generateNextCollectionName(nextToken, nextXPath); 
				eurl += "/" + colName; 
				
				mainbit: { 
					
					logger.debug("Looking for the next collection["+eurl+"]"); 
					
					nextCollection = DatabaseManager.getCollection(eurl, uname, passwd); 
					if(nextCollection == null) { 
						
						logger.debug(">> didn't find the last collection / eurl["+eurl+"]... creating previousUrl["+previousUrl+"] / collection name["+colName+"]"); 
						nextCollection = createCollection(previousUrl, colName); 
						logger.debug(">> Created the next collection["+ nextCollection +"] / url["+ previousUrl +"/"+ colName +"]"); 
						
					}
				}
				
				//** KLUDGE 2 
				if(stokenizer.hasMoreTokens()) { 
					moreTokens = true; 
					nextToken = stokenizer.nextToken(); 
					nextXPath = xpathTokenizer.nextToken(); 
					//logger.debug("2. nextToken ["+ nextToken +"]"); 
				}
				else { 
					moreTokens = false; 
				}
			}
		}
		catch(XMLDBException e) { 
			throw new SpittoonException(e.getMessage(), e); 
		}
		
		return nextCollection; 
	}
	
	private String generateCollectionNameR(String xpath, String bareXPath) { 
		
		
		StringTokenizer xtokenizer = new StringTokenizer(xpath, "/"); 
		StringTokenizer btokenizer = new StringTokenizer(bareXPath, "/"); 
		
		String eachx = null; 
		String eachb = null; 
		String eachPart = null; 
		StringBuilder sbuffer = new StringBuilder(); 
		
		while(xtokenizer.hasMoreTokens()) { 
			
			eachx = xtokenizer.nextToken(); 
			eachb = btokenizer.nextToken(); 
			
			eachPart = generateNextCollectionName(eachb, eachx); 
			logger.info("generateCollectionNameR:: eachx["+ eachx +"] > eachb["+ eachb +"] > eachPart["+ eachPart +"]"); 
			
			sbuffer.append(eachPart); 
			if(xtokenizer.hasMoreTokens()) 
				sbuffer.append("/"); 
		}
		String resultName = sbuffer.toString(); 
		logger.info("generateCollectionNameR:: result["+ resultName +"]"); 
		
		return resultName; 
	}
	private String generateNextCollectionName(String nextToken, String nextXPath) { 
		
		String yvalue = yankAttributeId(nextXPath); 
		logger.info("generating Collection name / nextXPath ["+ nextXPath +"] / yanked 'id' value ["+ yvalue +"] ..."); 
		
		return nextToken + "." + yvalue; 
	}
	private String yankAttributeId(String attributeString) { 
		
		logger.debug("yankAttributeId["+ attributeString +"]"); 
		
		//** test yanking 'id' attribute 
		//** make sure there's an 'id' attribute 
		int ii = attributeString.indexOf("@id"); 
		int jj = (ii + 3); 
		String id = null; 
		try { 
			attributeString.substring(ii, jj); 
		}
		catch(StringIndexOutOfBoundsException e) { 
			throw new SpittoonException("No 'id' attribute specified for ["+ attributeString +"]", e); 
		}
		
		//** find value for id attribute 
		String marker = attributeString.substring(jj); 
		//logger.info("yanking marker["+marker+"]"); 
		
		int kk = marker.indexOf("\""); 
		
		String subs = null; 
		if(kk == -1) { 
			
			kk = marker.indexOf("'"); 
			int ll = marker.indexOf("'", kk + 1); 
			subs = marker.substring(kk + 1, ll); 
			
			//logger.info("yanking >> ' > kk["+ kk +"] / ll["+ll+"] / marker["+marker+"] / subs["+subs+"]"); 
			
		}
		else { 
			
			int ll = marker.indexOf("\"", kk + 1); 
			subs = marker.substring(kk + 1, ll); 
			
			//logger.info("yanking >> \" > kk["+ kk +"] / ll["+ll+"] / marker["+marker+"] / subs["+subs+"]"); 
			
		}
		
		logger.info("yanked >> id["+id+"] / value["+subs+"]"); 
		return subs; 
	}
	private String generateDbUrl(String baseUrl, String xpath) { 
		
		return baseUrl + xpath; 
	}
	public String generateResourceName(IBob bob) {
		
		String tagName = bob.getTagName(); 
		String id = bob.getAttributeValue("id"); 
		
		return generateResourceName(tagName, id); 
	}
	public String generateResourceName(String tagName, String idValue) {
		
		StringBuilder sbuf = new StringBuilder(tagName); 
		sbuf.append("."); 
		sbuf.append(idValue); 
		
		String rname = sbuf.toString(); 
		logger.info("Generated resource name["+rname+"]"); 
		return rname; 
	}
	
	public boolean verifyXPathMapping(String xpath) { 
		
		java.util.Collection values = mappings.values(); 
		return values.contains(xpath); 
	}
	
	/**
	 * strips out any predicate(s)
	 */
	public String stripXPath(String xpath) { 
		
		/**
		 * this regular expression: \\[[^\\]]*\\]
		 * matches on all content between square brackets [ ], except a right square bracket. 
		 * The last condition allows multiple [ abc ] patterns in a single string 
		 */
		String bareXPath = xpath.replaceAll("\\[[^\\]]*\\]", ""); 
		if(bareXPath.indexOf("/") == bareXPath.length())
			bareXPath = bareXPath.substring(0, bareXPath.lastIndexOf("/")); 
		
		return bareXPath; 
	}
	
	
	public IBob loadBobFromXML(String xmlString) throws SpittoonException { 
		
		
		logger.debug("loadBobFromXML:: ["+ xmlString +"]"); 
		
		//** load a Bob object 
		InputStream istream = null; 
		try { 
			istream = new ByteArrayInputStream(xmlString.getBytes("UTF-8")); 
		}
		catch(UnsupportedEncodingException e) { 
			throw new SpittoonException(e); 
		}
		
		
	    //System.getProperties().setProperty("bob.home", props.getProperty("bob.home"));
	    //System.getProperties().setProperty("bob.base", props.getProperty("bob.base"));
	    //System.getProperties().setProperty("bob.gen", props.getProperty("bob.gen"));
	    //System.getProperties().setProperty("bob.end", props.getProperty("bob.gen"));
	    System.getProperties().setProperty("bob.home", "/Users/timothyw/Projects/Spittoon"); 
	    System.getProperties().setProperty("bob.base", "/Users/timothyw/Projects/Spittoon"); 
	    System.getProperties().setProperty("bob.gen", "/Users/timothyw/Projects/Spittoon/src/main/gen");
	    System.getProperties().setProperty("bob.end", ".xml");
	    
		IBob bob = Bob.loadS(istream, System.getProperty(Util.DEF)); 
		logger.debug("Loaded Bob from XML" + bob.toXML(false)); 
		
		return bob; 
	}
	
	
	
	public static void main(String args[]) { 
		
		/*System.getProperties().setProperty("bob.home", "/Users/timothyw/Projects/maven.trials/Spittoon"); 
	    System.getProperties().setProperty("bob.base", "/Users/timothyw/Projects/maven.trials/Spittoon"); 
	    System.getProperties().setProperty("bob.gen", "/Users/timothyw/Projects/maven.trials/Spittoon/src/main/gen");
	    System.getProperties().setProperty("bob.end", ".xml");

		String testme = "<group xmlns=\"com/interrupt/bookkeeping/users\" id=\"webkell\" name=\"Webkell\" owner=\"root\">" + 
			"<user id=\"root\" username=\"\" password=\"\" logintimeout=\"\" accountLevel=\"\" defaultGroup=\"\" authenticated=\"\"/>" + 
			"</group>"; 
		
	    InputStream istream = null; 
		try { 
			istream = new ByteArrayInputStream(testme.getBytes("UTF-8")); 
		}
		catch(UnsupportedEncodingException e) { 
			throw new SpittoonException(e); 
		}
		
		IBob bob = Bob.loadS(istream, "src/main/resources/bookkeeping.system.xml"); 
		System.out.println("Loaded Bob form XML" + bob.toXML(false)); 
		*/
		
		
		Spittoon spitt = new Spittoon(); 
		spitt.initialise(); 
		spitt.getCollection("xmldb:exist://abra.ms:8080/exist/xmlrpc/db/rootDir/system.main.system/groups.main.groups"); 
		
		//spitt.getCollection("xmldb:exist://abra.ms:8080/exist/xmlrpc/db/rootDir/system.main.system/aauthentication.main.aauthentication"); 
		
		spitt.getCollection("xmldb:exist://abra.ms:8080/exist/xmlrpc/db/rootDir/system.main.system/groups.main.groups/group.webkell"); 
		spitt.getCollection("xmldb:exist://abra.ms:8080/exist/xmlrpc/db/rootDir/system.main.system/groups.main.groups/group.webkell/bookkeeping.main.bookkeeping"); 
		//spitt.getCollection("xmldb:exist://abra.ms:8080/exist/xmlrpc/db/rootDir/system.main.system/groups.main.groups/group.seven/bookkeeping.main.bookkeeping/journals.main.journals"); 
		
	}
	
	private static void testStripXPath() { 
		
		//** testing stripXPath 
		/*String origXPath = "/system[ @id=\"thing\" ]/bookkeeping[ @attr=\"value\" ]/thing"; 
		
		Spittoon spittoon = new Spittoon(); 
		String bareXPath = spittoon.stripXPath(origXPath); 
		System.out.println(bareXPath); 
		*/
		
		//String testme = "/system[ @id=\"blahblah blah\" ]"; 
		String testme = "/system[ @id='blahblah blah' ]"; 
		System.out.println("Begin ["+ testme +"]"); 
		
		/*String sstring[] = testme.split("\\[[^\\]]*\\]"); 
		for (String string : sstring) {
			System.out.println(string);
		} 
		*/
		
	}
	private static void testYanking() { 
		
		
		String testme = "/system[ @id='blahblah blah' ]"; 
		
		//** test yanking 'id' attribute 
		//** make sure there's an 'id' attribute 
		int ii = testme.indexOf("@id"); 
		int jj = (ii + 3); 
		String id = testme.substring(ii, jj); 
		
		//** find value for id attribute 
		String marker = testme.substring(jj); 
		System.out.println("marker["+marker+"]"); 
		
		int kk = marker.indexOf("\""); 
		
		String subs = null; 
		if(kk == -1) { 
			
			kk = marker.indexOf("'"); 
			int ll = marker.indexOf("'", kk + 1); 
			subs = marker.substring(kk + 1, ll); 
			
			System.out.println(">> ' > kk["+ kk +"] / ll["+ll+"] / marker["+marker+"] / subs["+subs+"]"); 
			
		}
		else { 
			
			int ll = marker.indexOf("\"", kk + 1); 
			subs = marker.substring(kk + 1, ll); 
			
			System.out.println(">> \" > kk["+ kk +"] / ll["+ll+"] / marker["+marker+"] / subs["+subs+"]"); 
			
		}
		
		System.out.println("id["+id+"] / value["+subs+"]"); 
	}
	
	/*
	class FindParentBob implements IVisitor { 
		
		private Logger logger = Logger.getLogger(FindParentBob.class); 
		private String tagName = null; 
		private String attributeValue = null; 
		private IBob replacement = null; 
		
		public String getTagName() { return tagName; }
		public void setTagName(String tagName) { this.tagName = tagName; }
		public String getAttributeValue() { return attributeValue; }
		public void setAttributeValue(String attributeValue) { this.attributeValue = attributeValue; }
		public IBob getReplacement() { return replacement; }
		public void setReplacement(IBob replacement) { this.replacement = replacement; }
		
		private boolean acceptControl = true; 
		public void visit(IBob bob) { 
			
			if(acceptControl) { 
				
				String tname = bob.getTagName(); 
				String avalue = bob.getAttributeValue("id"); 
				
				logger.debug("FindParentBob.visit > comparing tagName["+tagName+"] <=> tname["+tname+"] / attributeValue["+attributeValue+"] <=> avalue["+avalue+"]"); 
				if((tagName.equals(tname)) && (attributeValue.equals(avalue))) { 
					
					logger.debug("FindParentBob.visit > replacing ["+bob.xpath(false)+"] with ["+replacement.toXML(false)+"]"); 
					bob.replace(replacement); 
					acceptControl = false; 
				}
			}
		}
	}
	*/
}

