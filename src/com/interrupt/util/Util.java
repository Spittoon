package com.interrupt.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.Logger;

public class Util {
	
	
	static Logger logger = Logger.getLogger(Util.class); 
	
	
	public static String loadTextFile(String resourceName) { 
		
		logger.debug("READING '"+ resourceName +"'..."); 
		
		InputStream istream = resourceName.getClass().getResourceAsStream(resourceName); 
		if(istream == null) { 
			
			istream = ClassLoader.getSystemResourceAsStream(resourceName);
			if(istream == null) { 
				ClassLoader cl = ClassLoader.getSystemClassLoader(); 
				istream = cl.getResourceAsStream(resourceName); 
			}
		}
		return Util.loadTextFile(istream); 
	}
	public static String loadTextFile(InputStream istream) { 
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(istream)); 
		StringBuilder sbuffer = new StringBuilder(); 
		try {
			
			char[] buf = new char[1024];
			int c;
			while ((c = reader.read(buf)) != -1) {
				
				String readData = String.valueOf(buf, 0, c); 
				sbuffer.append(readData); 
			}
		}
		catch (IOException e) {
		     System.err.println("IOException ["+ e.getMessage() +"]");
		}
		return sbuffer.toString(); 
	}
	
	public static InputStream getInputStreamFromString(String xmlString) throws UnsupportedEncodingException { 
		
		return new ByteArrayInputStream(xmlString.getBytes("UTF-8")); 
	}
	
}

