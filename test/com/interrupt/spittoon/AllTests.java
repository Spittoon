
package com.interrupt.spittoon;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class AllTests extends TestCase {
    
    public static Test suite() {
	
		TestSuite suite = new TestSuite();
		
		suite.addTest( com.interrupt.spittoon.TestSpittoon.suite() );
		
		return suite;
		
    }

}


