package com.interrupt.spittoon;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.exist.util.ByteArray;

import com.interrupt.bob.base.IBob;
import com.interrupt.util.Util;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class TestSpittoon extends TestCase {
	
	
	private Logger logger = Logger.getLogger(Spittoon.class); 
	private Spittoon spittoon = null;  
	
	public TestSpittoon(String name) { 
		super(name); 
	}
	public void setUp() throws SpittoonException { 
		
		spittoon = new Spittoon(); 
		spittoon.initialise(); 
		spittoon.setupRoot(); 
	}
	public void tearDown() throws SpittoonException { 
		spittoon.tearDownRoot(); 
	}
	
	/**
	 * java -classpath $CP 
	 * 		-Ddb.url=$DBURL 
	 * 		-Dexist.initdb="true" 
	 * 		-Dexist.home="." 
	 * 		-Dorg.xml.sax.features.validation="false" 
	 * 		-Djava.endorsed.dirs="lib/endorsed" 
	 * 		com.interrupt.bookkeeping.cc.bkell.Bkell
	 */
	
	/**
	 * make sure Spittoon can i) make a connection ii) throws an error if there's no connection 
	 */
	public void testConnect() throws SpittoonException { 
		
		//spittoon.initialise(); 
		
		boolean alive = spittoon.alive(); 
		assertTrue("Spittoon hasn't connected properly", alive); 
	}
	
	public void testStripXPath() { 
		
		String origXPath = "/system[ @id=\"thing\" ]/bookkeeping[ @attr=\"value\" ]"; 
		
		Spittoon spittoon = new Spittoon(); 
		String bareXPath = spittoon.stripXPath(origXPath); 
		assertEquals("/system/bookkeeping", bareXPath); 
		
		String anotherXPath = "/system[ @id=\"thing\" ]/bookkeeping[ @attr=\"value\" ]/thing"; 
		String anotherBare = spittoon.stripXPath(anotherXPath); 
		assertEquals("/system/bookkeeping/thing", anotherBare); 
	}
	
	
	/**
	 * create a shallow XML file with a depth of 1
	 */
	public void testCreateD1AAA() throws SpittoonException { 
		
		String axml = Util.loadTextFile("setup.aauthentication.xml"); 
		String gxml = Util.loadTextFile("setup.groups.xml"); 
		//logger.debug("Loaded XML..."); 
		//logger.debug(xml); 
		
		//String gxpath = "/system[ @id='main.system' ]/groups[ @id='main.groups' ]"; 
		String axpath = "/system[ @id='main.system' ]"; 
		logger.debug("Aauthentication XPath["+ axpath +"]"); 
		String aurl = spittoon.getAauthDbUrl();  
		spittoon.createR(aurl, axpath, axml); 
		
		//String axpath = "/system[ @id='main.system' ]/aauthentication[ @id='main.authentication' ]"; 
		String gxpath = "/system[ @id='main.system' ]"; 
		logger.debug("Groups XPath["+ gxpath +"]"); 
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, gxpath, gxml); 
		
	}
	
	public void testCreateDRecursiveAAACCC() throws SpittoonException { 
		
		String xpath = "/system[ @id='main.system' ]"; 
		String xml = Util.loadTextFile("bookkeeping.xml"); 
		logger.debug("XPath["+ xpath +"]"); 
		logger.debug("XML..."); 
		logger.debug(xml); 
		
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, xpath, xml); 
		
	}
	
	public void testRetrieve() { 
		
		//** preamble 
		String xpath = "/system[ @id='main.system' ]"; 
		String xml = Util.loadTextFile("bookkeeping.xml"); 
		logger.debug("XPath["+ xpath +"]"); 
		logger.debug("XML..."); 
		logger.debug(xml); 
		
		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, xpath, xml); 
		
		
		//**
		String xpath2 = "/system[ @id='main.system' ]/groups[ @id='main.groups' ]/group[ @id='webkell' and @name='Webkell' and @owner='root' ]"; 
		IBob xbob = spittoon.retrieve(gurl, xpath2, false); 
		assertNotNull(xbob); 
		
	}
	
	
	public void testRetrieveRecursive() { 
		
		
		//System.setProperty("org.apache.xerces.xni.parser.XMLParserConfiguration",
		//    "org.apache.xerces.parsers.XMLGrammarCachingConfiguration");
		//System.setProperty("java.endorsed.dirs", "lib/endorsed"); 
		
		//** preamble 
		String xpath = "/system[ @id='main.system' ]"; 
		String xml = Util.loadTextFile("bookkeeping.xml"); 
		logger.debug("XPath["+ xpath +"]"); 
		logger.debug("XML..."); 
		logger.debug(xml); 
		

		String gurl = spittoon.getGroupsDbUrl(); 
		spittoon.createR(gurl, xpath, xml); 
		
		//**
		String xpath2 = "/system[ @id='main.system' ]"; 
		
		
		IBob resultBob = spittoon.retrieve(gurl, xpath2, true); 
		logger.debug("FINAL RESULT..."); 
		logger.debug(resultBob.toXML(false)); 
		assertNotNull(resultBob); 
		
	}
	
	

    public static Test suite() {
		
    	TestSuite suite = new TestSuite();	
		
    	suite.addTest( new TestSpittoon("testConnect") ); 
    	suite.addTest( new TestSpittoon("testStripXPath") ); 
    	suite.addTest( new TestSpittoon("testCreateD1AAA") ); 
    	suite.addTest( new TestSpittoon("testCreateDRecursiveAAACCC") ); 
    	
		suite.addTest( new TestSpittoon("testRetrieve") ); 
    	suite.addTest( new TestSpittoon("testRetrieveRecursive") ); 
		
		return suite; 
		
    }
}

